import CheckCodesAlert from "../views/ActivateCashbackAlert";
import Vue from 'vue'
import Vuex from 'vuex'

browser.runtime.onMessage.addListener(shop => {
	saveShopToCookie(JSON.stringify(shop));
	if (shop && shop.name) {
		showActivateCashbackAlert(shop);
    }
});

readOrFetchShopInfo();
addPluginInstalledInfo();


function readOrFetchShopInfo() {
	const shopString = readShopFromCookie();
	if (!shopString) {
		chrome.runtime.sendMessage({
			needed: 'pageLoaded',
			data: {
				url: window.location.host,
			}
		}, function () {});
	} else {
		const shop = JSON.parse(shopString);
		if (shop && shop.name) {
			showActivateCashbackAlert(shop);
		}
	}
}

function saveShopToCookie(value) {
	const date = new Date();
	date.setTime(date.getTime()+(60*60*1000));
	const expires = "; expires="+date.toGMTString();
	document.cookie = "cashgoback_shop="+ value + expires+"; path=/";
}

function readShopFromCookie() {
	let nameEQ = "cashgoback_shop=";
	let ca = document.cookie.split(';');
	for (let i=0;i < ca.length;i++) {
		let c = ca[i];
		while (c.charAt(0) === ' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function addPluginInstalledInfo() {
    if (window.location.href.indexOf('cashgoback.pl') !== -1) {
        const insertedSpan = document.createElement('span');
        insertedSpan.setAttribute("id", "cashgoback-plugin-installed");
        document.body.appendChild( insertedSpan );
    }
}

function loadFont() {
    const importLoaded = function (importNode, event) {
        importNode.import = event.target.import;
    };
    const importNode = document.createElement('link');
    importNode.rel= 'stylesheet';
    importNode.href= 'https://fonts.googleapis.com/css2?family=Poppins&display=swap';
    importNode.onload = importLoaded.bind(this, importNode);
    document.head.appendChild(importNode);
}

function showActivateCashbackAlert(shop) {
    Vue.use(Vuex);
    loadFont();
    const stylesContainer = document.createElement('div');
    stylesContainer.innerHTML = '<link href="https://cashgoback.pl/css/extension/alert.css?v=2.1.0" rel="stylesheet">';
    const insertedBanner = document.createElement('div');
    insertedBanner.setAttribute("id", "cashgoback_alert");
    const shadowRoot = insertedBanner.attachShadow({mode: 'open'});

    const vueContainer = document.createElement('div');
    vueContainer.setAttribute("id", "cashgoback_alert_container");
    shadowRoot.appendChild(stylesContainer);
    shadowRoot.appendChild(vueContainer);
    document.body.appendChild( insertedBanner );
    let element = document.querySelector('#cashgoback_alert').shadowRoot.querySelector('#cashgoback_alert_container');

    const store = new Vuex.Store({
        state: {
            shop: shop,
        },
        mutations: {
            increment (state) {
                state.count++
            }
        }
    });
    /* eslint-disable no-new */
    new Vue({
        el: element,
        store,
        render: h => h(CheckCodesAlert)
    });
}
