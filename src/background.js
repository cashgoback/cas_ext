import axios from "axios";

chrome.runtime.onInstalled.addListener((details) => {
    switch (details.reason) {
        case 'install':
            openExtensionInstalledPage();
            break;
        case 'update':
            break;
        case 'chrome_update':
        case 'shared_module_update':
        default:
            break;
    }
});

chrome.browserAction.onClicked.addListener(function() {
    chrome.tabs.create({
        url: "https://www.cashgoback.pl/"
    });
});

function openExtensionInstalledPage() {
    chrome.tabs.create({url: "https://www.cashgoback.pl"}, function () {});
}

browser.runtime.onMessage.addListener(function (request) {
    if (request.needed === 'pageLoaded') {
        checkShopUrl(request.data.url);
    }

    return true;
});

function checkShopUrl(url) {
    const apiUrl = 'https://cashgoback.pl/api/plugins/v2/shop?url=' + encodeURIComponent(url);

    axios.get(apiUrl).then((result) => {
        sendShopToContentScript(result.data);
    });
}

function sendShopToContentScript(shop) {
    chrome.tabs.query({active: true, lastFocusedWindow: true}, tabs => {
        browser.tabs.sendMessage(tabs[0].id, shop);
    });
}

